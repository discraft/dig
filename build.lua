local tArgs = { ... }

-- Mine in a quarry pattern until we hit something we can't dig
local size = 16
local returnHeight = tArgs[2] or 90
local returnOffset = tArgs[1] and (returnHeight - tonumber(tArgs[1])) or 0

local depth = 0
local unloaded = 0
local collected = 0

local xPos, zPos = 0, 0
local xDir, zDir = 0, 1

local goTo -- Filled in further down
local refuel -- Filled in further down

local function isLiquid()
    local _, data = turtle.inspectDown()
    local block = data.name
    if block == "minecraft:water" or block == "minecraft:lava" then
        turtle.select(4)
        if turtle.getItemCount() == 1 then
            turtle.select(3)
            turtle.placeUp()
            turtle.select(4)
            turtle.suckUp(63)
            turtle.select(3)
            turtle.digUp()
        end
        turtle.select(4)
        turtle.placeDown()
        return true
    else
        return false
    end
end

local function returnSupplies()
    if turtle.getItemCount(16) ~= 0 then
        turtle.select(2)
        turtle.placeUp()
        for i = 16,5,-1 do
            turtle.select(i)
            turtle.dropUp(64)
        end
        turtle.select(2)
        turtle.digUp()
    end
end


local function refuel()
    if turtle.getFuelLevel() < 64 then
        turtle.select(1)
        turtle.placeUp()
        turtle.suckUp(16)
        turtle.refuel()
        turtle.digUp()
    end
    return true
end

local function tryForwards()
    refuel()   
	isLiquid()
    while not turtle.forward() do
        if turtle.detect() then
            if turtle.dig() then
                returnSupplies()    
            else
                return false
            end
        end
    end

    xPos = xPos + xDir
    zPos = zPos + zDir
    return true
end

local function tryDown()
    refuel()
	isLiquid()
    while not turtle.down() do
        if turtle.detectDown() then
            if turtle.digDown() then
                returnSupplies()
            else
                return false
            end
        end
    end
    depth = depth + 1
    return true
end

local function turnLeft()
    turtle.turnLeft()
    xDir, zDir = -zDir, xDir
end

local function turnRight()
    turtle.turnRight()
    xDir, zDir = zDir, -xDir
end

function goTo(x, y, z, xd, zd)
    while depth > y do
        if turtle.up() then
            depth = depth - 1
        elseif turtle.digUp() then
            returnSupplies()
        else
            sleep(0.5)
        end
    end

    if xPos > x then
        while xDir ~= -1 do
            turnLeft()
        end
        while xPos > x do
            if turtle.forward() then
                xPos = xPos - 1
            elseif turtle.dig() or turtle.attack() then
                returnSupplies()
            else
                sleep(0.5)
            end
        end
    elseif xPos < x then
        while xDir ~= 1 do
            turnLeft()
        end
        while xPos < x do
            if turtle.forward() then
                xPos = xPos + 1
            elseif turtle.dig() or turtle.attack() then
                returnSupplies()
            else
                sleep(0.5)
            end
        end
    end

    if zPos > z then
        while zDir ~= -1 do
            turnLeft()
        end
        while zPos > z do
            if turtle.forward() then
                zPos = zPos - 1
            elseif turtle.dig() or turtle.attack() then
                returnSupplies()
            else
                sleep(0.5)
            end
        end
    elseif zPos < z then
        while zDir ~= 1 do
            turnLeft()
        end
        while zPos < z do
            if turtle.forward() then
                zPos = zPos + 1
            elseif turtle.dig() or turtle.attack() then
                returnSupplies()
            else
                sleep(0.5)
            end
        end
    end

    while depth < y do
        if turtle.down() then
            depth = depth + 1
        elseif turtle.digDown() or turtle.attackDown() then
            returnSupplies()
        else
            sleep(0.5)
        end
    end

    while zDir ~= zd or xDir ~= xd do
        turnLeft()
    end
end

if not refuel() then
    print("Out of Fuel")
    return
end

print("Excavating...")

turtle.select(1)

local alternate = 0
local done = false
while not done do
    for n = 1, size do
        for _ = 1, size - 1 do
            if not tryForwards() then
                done = true
                break
            end
        end
        if done then
            break
        end
        if n < size then
            if math.fmod(n + alternate, 2) == 0 then
                turnLeft()
                if not tryForwards() then
                    done = true
                    break
                end
                turnLeft()
            else
                turnRight()
                if not tryForwards() then
                    done = true
                    break
                end
                turnRight()
            end
        end
    end
    if done then
        break
    end

    if size > 1 then
        if math.fmod(size, 2) == 0 then
            turnRight()
        else
            if alternate == 0 then
                turnLeft()
            else
                turnRight()
            end
            alternate = 1 - alternate
        end
    end

    if not tryDown() then
        done = true
        break
    end
end

print("Returning to surface...")

-- Return to where we started
refuel()
goTo(0, 0, 0, 0, -1)
goTo(0, (-1*returnOffset), 0, 0, 1)

-- Empty inv when done
turtle.select(2)
turtle.placeUp()
for i = 16,5,-1 do
    turtle.select(i)
    turtle.dropUp(64)
end
turtle.select(2)
turtle.digUp()

print("Mined " .. collected + unloaded .. " items total.")
